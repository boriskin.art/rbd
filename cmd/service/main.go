package main

import (
	"net/http"
	"time"

	"practice_4/config"
	"practice_4/pkg/db"

	batchR "practice_4/internal/repository/batch"
	countryR "practice_4/internal/repository/country"
	factManufacturingR "practice_4/internal/repository/fact_manufacturing"
	factoryR "practice_4/internal/repository/factory"
	furnitureR "practice_4/internal/repository/furniture"
	furnitureSubTypeR "practice_4/internal/repository/furniture_sub_type"
	furnitureTypeR "practice_4/internal/repository/furniture_type"
	gostR "practice_4/internal/repository/gost"
	machineR "practice_4/internal/repository/machine"
	machineTypeR "practice_4/internal/repository/machine_type"
	workerR "practice_4/internal/repository/worker"

	batchS "practice_4/internal/services/batch"
	countryS "practice_4/internal/services/country"
	factManufacturingS "practice_4/internal/services/fact_manufacturing"
	factoryS "practice_4/internal/services/factory"
	furnitureS "practice_4/internal/services/furniture"
	furnitureSubTypeS "practice_4/internal/services/furniture_sub_type"
	furnitureTypeS "practice_4/internal/services/furniture_type"
	gostS "practice_4/internal/services/gost"
	machineS "practice_4/internal/services/machine"
	machineTypeS "practice_4/internal/services/machine_type"
	workerS "practice_4/internal/services/worker"

	batchH "practice_4/internal/handlers/batch"
	countryH "practice_4/internal/handlers/country"
	factManufacturingH "practice_4/internal/handlers/fact_manufacturing"
	factoryH "practice_4/internal/handlers/factory"
	furnitureH "practice_4/internal/handlers/furniture"
	furnitureSubTypeH "practice_4/internal/handlers/furniture_sub_type"
	furnitureTypeH "practice_4/internal/handlers/furniture_type"
	gostH "practice_4/internal/handlers/gost"
	machineH "practice_4/internal/handlers/machine"
	machineTypeH "practice_4/internal/handlers/machine_type"
	workerH "practice_4/internal/handlers/worker"

	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

func main() {
	// Gorilla router
	r := mux.NewRouter()

	// Init config
	config.InitConfig()

	// Init database
	pg := db.PostgresConnection()

	// Repositories
	gostRepo    		  := gostR.New(pg)
	countryRepo 		  := countryR.New(pg)
	batchRepo   		  := batchR.New(pg)
	furnitureRepo         := furnitureR.New(pg)
	furnitureTypeRepo     := furnitureTypeR.New(pg)
	furnitureSubTypeRepo  := furnitureSubTypeR.New(pg)
	machineRepo           := machineR.New(pg)
	machineTypeRepo       := machineTypeR.New(pg)
	workerRepo            := workerR.New(pg)
	factoryRepo           := factoryR.New(pg)
	factManufacturingRepo := factManufacturingR.New(pg)

	// Services
	gostService              := gostS.New(gostRepo)
	countryService           := countryS.New(countryRepo)
	batchService             := batchS.New(batchRepo)
	furnitureService         := furnitureS.New(furnitureRepo)
	furnitureTypeService     := furnitureTypeS.New(furnitureTypeRepo)
	furnitureSubTypeService  := furnitureSubTypeS.New(furnitureSubTypeRepo)
	machineService           := machineS.New(machineRepo)
	machineTypeService       := machineTypeS.New(machineTypeRepo)
	workerService            := workerS.New(workerRepo)
	factoryService           := factoryS.New(factoryRepo)
	factManufacturingService := factManufacturingS.New(factManufacturingRepo)

	// Handlers
	gostH.New(r, gostService)
	countryH.New(r, countryService)
	batchH.New(r, batchService)
	furnitureH.New(r, furnitureService)
	furnitureTypeH.New(r, furnitureTypeService)
	furnitureSubTypeH.New(r, furnitureSubTypeService)
	machineH.New(r, machineService)
	machineTypeH.New(r, machineTypeService)
	workerH.New(r, workerService)
	factoryH.New(r, factoryService)
	factManufacturingH.New(r, factManufacturingService)

	srv := &http.Server{
		Addr:         viper.GetString("port"),
		Handler:      r,
		ReadTimeout:  30 * time.Second,
		WriteTimeout: 30 * time.Second,
	}

	logrus.Info("Starting server at http://" + viper.GetString("domain"))

	if err := srv.ListenAndServe(); err != nil {
		logrus.Fatal("Server error:", err)
	}

}