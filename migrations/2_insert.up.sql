insert into dim_gost(gost_description) values ('gost desc 1');
insert into dim_gost(gost_description) values ('gost desc 2');
insert into dim_gost(gost_description) values ('gost desc 3');

insert into dim_batch(batch_name) values ('Batch 1');
insert into dim_batch(batch_name) values ('Batch 2');

insert into dim_country(country_name) values ('Moscow');
insert into dim_country(country_name) values ('Kaliningrad');
insert into dim_country(country_name) values ('Podolsk');

insert into dim_factory(factory_name, country_code) values ('Fact 1', 1);
insert into dim_factory(factory_name, country_code) values ('Fact 2', 1);
insert into dim_factory(factory_name, country_code) values ('Fact 3', 2);
insert into dim_factory(factory_name, country_code) values ('Fact 4', 3);

insert into dim_worker(worker_name, factory_code) VALUES ('Oleg #177', 1);
insert into dim_worker(worker_name, factory_code) VALUES ('Mihail #111', 1);
insert into dim_worker(worker_name, factory_code) VALUES ('Alex #566', 2);
insert into dim_worker(worker_name, factory_code) VALUES ('Dmitriy #78', 3);
insert into dim_worker(worker_name, factory_code) VALUES ('Pasha #987', 4);
insert into dim_worker(worker_name, factory_code) VALUES ('Grisha #397', 4);

insert into dim_machine_type(machine_type_description) values ('Machine type 1');
insert into dim_machine_type(machine_type_description) values ('Machine type 2');
insert into dim_machine_type(machine_type_description) values ('Machine type 3');

insert into dim_machine(date_of_purchase, machine_name, machine_type_code) VALUES ('2021-07-15', 'MACHINE 1', 1);
insert into dim_machine(date_of_purchase, machine_name, machine_type_code) VALUES ('2020-01-10', 'MACHINE 2', 2);
insert into dim_machine(date_of_purchase, machine_name, machine_type_code) VALUES ('2010-12-12', 'MACHINE 3', 3);
insert into dim_machine(date_of_purchase, machine_name, machine_type_code) VALUES ('2010-12-12', 'MACHINE 4', 3);

insert into dim_furniture_type(furniture_type_name) values ('Type 1');
insert into dim_furniture_type(furniture_type_name) values ('Type 2');

insert into dim_furniture_sub_type(furniture_sub_type_name, furniture_type_code) VALUES ('Sub Type 1', 1);
insert into dim_furniture_sub_type(furniture_sub_type_name, furniture_type_code) VALUES ('Sub Type 2', 2);

insert into dim_furniture(furniture_name, furniture_material, furniture_sub_type_code, gost_id)
VALUES ('Furniture 1', 'Material 1', 1, 1);
insert into dim_furniture(furniture_name, furniture_material, furniture_sub_type_code, gost_id)
VALUES ('Furniture 2', 'Material 1', 2, 1);
insert into dim_furniture(furniture_name, furniture_material, furniture_sub_type_code, gost_id)
VALUES ('Furniture 3', 'Material 2', 2, 2);

insert into fact_manufacturing(
        batch_code, furniture_code, machine_code, accepted_products,
        rejected_products, elapsed_time, gost_id)
values (1, 1, 1, 100, 3, '15:00:00', 1);
insert into fact_manufacturing(
    batch_code, furniture_code, machine_code, accepted_products,
    rejected_products, elapsed_time, gost_id)
values (2, 2, 2, 1000, 6, '23:00:00', 2);
insert into fact_manufacturing(
    batch_code, furniture_code, machine_code, accepted_products,
    rejected_products, elapsed_time, gost_id)
values (2, 2, 1, 500, 17, '9:00:00', 2);