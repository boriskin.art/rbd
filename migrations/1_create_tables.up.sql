create table if not exists dim_gost (
    gost_id serial primary key,
    gost_description text not null
);

create table if not exists dim_furniture_type (
    furniture_type_code serial primary key,
    furniture_type_name text not null
);

create table if not exists dim_furniture_sub_type (
    furniture_sub_type_code serial primary key,
    furniture_sub_type_name text not null,
    furniture_type_code integer REFERENCES dim_furniture_type(furniture_type_code) ON DELETE CASCADE
);

create table if not exists dim_furniture (
    furniture_code serial primary key,
    furniture_name text not null,
    furniture_material text not null,
    furniture_sub_type_code integer REFERENCES dim_furniture_sub_type(furniture_sub_type_code) ON DELETE CASCADE,
    gost_id integer REFERENCES dim_gost(gost_id) ON DELETE CASCADE
);

create table if not exists dim_batch (
    batch_code serial primary key,
    batch_name text not null
);

create table if not exists dim_country (
    country_code serial primary key,
    country_name text not null
);

create table if not exists dim_factory (
    factory_code serial primary key,
    factory_name text not null,
    country_code integer REFERENCES dim_country(country_code) ON DELETE CASCADE
);

create table if not exists dim_worker (
    worker_code serial primary key,
    worker_name text not null,
    factory_code integer REFERENCES dim_factory(factory_code) ON DELETE CASCADE
);

create table if not exists dim_machine_type(
    machine_type_code serial primary key,
    machine_type_description text not null
);

create table if not exists dim_machine (
    machine_code serial primary key,
    date_of_purchase date not null,
    machine_name text not null,
    machine_type_code integer REFERENCES dim_machine_type(machine_type_code) ON DELETE CASCADE
);

create table if not exists fact_manufacturing (
    id serial primary key,
    batch_code integer REFERENCES dim_batch(batch_code) ON DELETE CASCADE,
    furniture_code integer REFERENCES dim_furniture(furniture_code) ON DELETE CASCADE,
    machine_code integer REFERENCES dim_machine(machine_code) ON DELETE CASCADE,
    accepted_products integer not null,
    rejected_products integer not null,
    elapsed_time text not null,
    gost_id integer REFERENCES dim_gost(gost_id) ON DELETE CASCADE
);