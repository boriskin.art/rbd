select * from fact_manufacturing;
select * from dim_furniture;
select * from dim_worker;
select * from dim_factory;

select furniture_type_name from dim_furniture_type;
select accepted_products, rejected_products from fact_manufacturing;

SELECT gost_id, gost_description FROM dim_gost WHERE gost_id = 1;

select * from dim_batch where batch_code = 1;
select * from dim_worker where worker_code < 3;
select * from dim_country where country_name like '%os%';
select * from dim_factory where factory_code = 1 or factory_name like 'Fact _';

select * from fact_manufacturing order by rejected_products;
select * from fact_manufacturing order by elapsed_time desc;

select fm.accepted_products, db.batch_code, db.batch_name from fact_manufacturing as fm inner join dim_batch db on db.batch_code = fm.batch_code;

select fm.accepted_products, db.batch_code, db.batch_name from fact_manufacturing as fm right join dim_batch db on db.batch_code = fm.batch_code;

select fm.accepted_products, db.batch_code, db.batch_name from fact_manufacturing as fm left join dim_batch db on db.batch_code = fm.batch_code;