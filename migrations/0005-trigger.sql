CREATE OR REPLACE FUNCTION insert_trigger()
    RETURNS trigger AS
$$
BEGIN
    INSERT INTO dim_country(country_name)
    VALUES('test trigger');

    RETURN NEW;
END;
$$
    LANGUAGE 'plpgsql';

CREATE TRIGGER insert_trigger
    AFTER INSERT
    ON dim_factory
    FOR EACH ROW
EXECUTE PROCEDURE insert_trigger();

insert into dim_factory(factory_name, country_code) VALUES ('test', 1);