# Build binary with downloaded go modules
FROM golang:1.16.7-buster as build

# Copy files
RUN mkdir /home/RBD
COPY ./go.mod /home/RBD
WORKDIR /home/RBD

# Download go modules to cache
RUN go mod download
COPY . /home/RBD

# Build application
RUN CGO_ENABLED=0 go build -o RBD ./cmd/service/*

# Start app in low-size alpine image
FROM alpine:3.14.1

RUN mkdir RBD

# Copy files from build step
WORKDIR RBD
COPY --from=build /home/RBD/RBD .
RUN mkdir config
COPY --from=build /home/RBD/config ./config

# Start app
CMD ["./RBD"]
