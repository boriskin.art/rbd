package config

import (
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"os"
)

// InitConfig initialize configs from yml file via viper and set some from env
func InitConfig() {
	viper.AddConfigPath("./config")
	viper.SetConfigName("config")

	if err := viper.ReadInConfig(); err != nil {
		logrus.Fatal("Config parse error:", err)
	}

	// DB Settings
	viper.Set("db.host", os.Getenv("RBD_DB_HOST"))
	viper.Set("db.port", os.Getenv("RBD_DB_PORT"))
	viper.Set("db.dbname", os.Getenv("RBD_DB_DBNAME"))
	viper.Set("db.user", os.Getenv("RBD_DB_USER"))
	viper.Set("db.password", os.Getenv("RBD_DB_PASSWORD"))
	viper.Set("db.sslmode", os.Getenv("RBD_DB_SSLMODE"))
}
