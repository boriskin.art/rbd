package gost

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"net/http"
	"practice_4/internal"
	"practice_4/internal/models"
	"practice_4/internal/services/gost"
	"strconv"
)

type Handler struct {
	gostService gost.Service
}


func New(router *mux.Router, gostService gost.Service) {
	handler := &Handler{
		gostService: gostService,
	}

	g := router.PathPrefix("/gost").Subrouter()

	// Routes for gost model
	g.HandleFunc("/create", handler.Create).Methods("POST")
	g.HandleFunc("/get-by-id/{id:[0-9]+}", handler.GetByID).Methods("GET")
	g.HandleFunc("/get-by-name/{name}", handler.GetByName).Methods("GET")
	g.HandleFunc("/get", handler.GetAll).Methods("GET")
	g.HandleFunc("/delete/{id:[0-9]+}", handler.Delete).Methods("GET")
}


func (h *Handler) Create(w http.ResponseWriter, r *http.Request) {
	g := models.Gost{}

	err := json.NewDecoder(r.Body).Decode(&g)
	if err != nil {
		http.Error(w, err.Error(), 400)
		return
	}

	result, err := h.gostService.Create(g)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	err = json.NewEncoder(w).Encode(internal.Response{
		Status: "ok",
		Result: internal.Result{ID: result},
	})
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
}

func (h *Handler) GetAll(w http.ResponseWriter, r *http.Request) {
	result, err := h.gostService.GetAll()
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	err = json.NewEncoder(w).Encode(internal.Response{
		Status: "ok",
		Result: result,
	})
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
}

func (h *Handler) GetByID(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.Atoi(mux.Vars(r)["id"])
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	result, err := h.gostService.GetByID(id)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	err = json.NewEncoder(w).Encode(internal.Response{
		Status: "ok",
		Result: result,
	})
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
}

func (h *Handler) GetByName(w http.ResponseWriter, r *http.Request) {
	name := mux.Vars(r)["name"]

	result, err := h.gostService.GetByName(name)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	err = json.NewEncoder(w).Encode(internal.Response{
		Status: "ok",
		Result: result,
	})
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
}

func (h *Handler) Delete(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.Atoi(mux.Vars(r)["id"])
	if err != nil {
		http.Error(w, "invalid request", 400)
	}

	err = h.gostService.Delete(id)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	err = json.NewEncoder(w).Encode(internal.Response{
		Status: "ok",
		Result: internal.Result{ID: id},
	})
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
}