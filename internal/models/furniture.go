package models

type Furniture struct {
	Code 		int		`json:"code"`
	Name 		string	`json:"name"`
	Material 	string	`json:"material"`
	SubTypeCode int		`json:"sub_type_code"`
	Gost 		int		`json:"gost"`
}