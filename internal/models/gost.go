package models

type Gost struct {
	ID 			int    `json:"id"`
	Description string `json:"description"`
}

type GostResponse struct {
	ID int `json:"id"`
}
