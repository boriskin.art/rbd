package models

type Factory struct {
	Code 		int 	`json:"code"`
	Name 		string	`json:"name"`
	CountryCode int		`json:"country_code"`
}
