package models

type Worker struct {
	Code 		int		`json:"code"`
	Name 		string	`json:"name"`
	FactoryCode int		`json:"factory_code"`
}
