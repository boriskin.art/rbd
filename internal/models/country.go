package models

type Country struct {
	Code int	 `json:"code"`
	Name string  `json:"name"`
}
