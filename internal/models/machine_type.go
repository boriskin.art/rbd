package models

type MachineType struct {
	TypeCode int	`json:"type_code"`
	TypeName string	`json:"type_name"`
}
