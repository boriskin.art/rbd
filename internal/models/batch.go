package models

type Batch struct {
	Code int	 `json:"code"`
	Name string	 `json:"name"`
}
