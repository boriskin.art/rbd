package models

type FurnitureSubType struct {
	SubTypeCode int	    `json:"sub_type_code"`
	SubTypeName string	`json:"sub_type_name"`
	TypeCode    int     `json:"type_code"`
}
