package models

import "time"

type Machine struct {
	Code 			int			`json:"code"`
	DateOfPurchase 	time.Time	`json:"date_of_purchase"`
	Name 			string		`json:"name"`
	TypeCode 		int			`json:"type_code"`
}
