package models

type FactManufacturing struct {
	ID 					int		`json:"id"`
	BatchCode 			int 	`json:"batch_code"`
	FurnitureCode 		int 	`json:"furniture_code"`
	MachineCode 		int		`json:"machine_code"`
	AcceptedProducts 	int		`json:"accepted_products"`
	RejectedProducts 	int		`json:"rejected_products"`
	ElapsedTime			string	`json:"elapsed_time"`
	GostID	 			int		`json:"gost_id"`
}
