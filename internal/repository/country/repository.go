package country

import (
	"database/sql"
	"fmt"
	"practice_4/internal/models"
)

type Repository interface {
	Create(c models.Country) (id int, err error)
	GetByID(id int) (c *models.Country, err error)
	GetByName(name string) (c *models.Country, err error)
	GetAll() (c []*models.Country, err error)
	Delete(id int) (err error)
}

type repository struct {
	db *sql.DB
}

func New(db *sql.DB) Repository {
	return &repository{
		db: db,
	}
}

func (r *repository) Create(c models.Country) (id int, err error) {
	err = r.db.QueryRow(`INSERT INTO dim_country(country_name) VALUES($1) RETURNING country_code`, c.Name).
		Scan(&id)
	return
}

func (r *repository) GetByID(id int) (c *models.Country, err error) {
	c = &models.Country{}
	err = r.db.QueryRow(`SELECT country_code, country_name FROM dim_country WHERE country_code = $1`, id).
		Scan(&c.Code, &c.Name)
	if err == sql.ErrNoRows {
		return nil, nil
	}
	return
}

func (r *repository) GetByName(name string) (c *models.Country, err error) {
	c = &models.Country{}
	err = r.db.QueryRow(`SELECT country_code, country_name FROM dim_country WHERE country_name = $1`, name).
		Scan(&c.Code, &c.Name)
	if err == sql.ErrNoRows {
		return nil, nil
	}
	return
}

func (r *repository) GetAll() (c []*models.Country, err error) {
	rows, err := r.db.Query(`SELECT country_code, country_name FROM dim_country`)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var data models.Country
		err = rows.Scan(&data.Code, &data.Name)
		if err != nil {
			return nil, err
		}
		c = append(c, &data)
	}
	return
}

func (r *repository) Delete(id int) (err error) {
	result, err := r.db.Exec(`DELETE FROM dim_country WHERE country_code = $1`, id)
	if count, _ := result.RowsAffected(); count == 0 {
		return fmt.Errorf("no rows found")
	}
	return
}