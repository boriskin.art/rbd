package furniture

import (
	"database/sql"
	"fmt"
	"practice_4/internal/models"
)

type Repository interface {
	Create(f models.Furniture) (id int, err error)
	GetByID(id int) (f *models.Furniture, err error)
	GetAll() (f []*models.Furniture, err error)
	Delete(id int) (err error)
}

type repository struct {
	db *sql.DB
}

func New(db *sql.DB) Repository {
	return &repository{
		db: db,
	}
}

func (r *repository) Create(f models.Furniture) (id int, err error) {
	err = r.db.QueryRow(
		`INSERT INTO dim_furniture(furniture_name, furniture_material, furniture_sub_type_code, gost_id) 
				VALUES($1, $2, $3, $4) RETURNING furniture_code`,
		f.Name, f.Material, f.SubTypeCode, f.Gost).
		Scan(&id)
	return
}

func (r *repository) GetByID(id int) (f *models.Furniture, err error) {
	f = &models.Furniture{}
	err = r.db.QueryRow(
		`SELECT furniture_code, furniture_name, furniture_material, furniture_sub_type_code, gost_id 
				FROM dim_furniture WHERE furniture_code = $1`, id).
		Scan(&f.Code, &f.Name, &f.Material, &f.SubTypeCode, &f.Gost)
	if err == sql.ErrNoRows {
		return nil, nil
	}
	return
}

func (r *repository) GetAll() (f []*models.Furniture, err error) {
	rows, err := r.db.Query(
		`SELECT furniture_code, furniture_name, furniture_material, furniture_sub_type_code, gost_id FROM dim_furniture`)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var data models.Furniture
		err = rows.Scan(&data.Code, &data.Name, &data.Material, &data.SubTypeCode, &data.Gost)
		if err != nil {
			return nil, err
		}
		f = append(f, &data)
	}
	return
}

func (r *repository) Delete(id int) (err error) {
	result, err := r.db.Exec(`DELETE FROM dim_furniture WHERE furniture_code = $1`, id)
	if count, _ := result.RowsAffected(); count == 0 {
		return fmt.Errorf("no rows found")
	}
	return
}