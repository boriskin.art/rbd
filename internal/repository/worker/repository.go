package worker

import (
	"database/sql"
	"fmt"
	"practice_4/internal/models"
)

type Repository interface {
	Create(w models.Worker) (id int, err error)
	GetByID(id int) (w *models.Worker, err error)
	GetByFactoryCode(id int) (w []*models.Worker, err error)
	GetAll() (w []*models.Worker, err error)
	Delete(id int) (err error)
}

type repository struct {
	db *sql.DB
}

func New(db *sql.DB) Repository {
	return &repository{
		db: db,
	}
}

func (r *repository) Create(w models.Worker) (id int, err error) {
	err = r.db.QueryRow(`INSERT INTO dim_worker(worker_name, factory_code) VALUES($1, $2) RETURNING worker_code`, w.Name, w.FactoryCode).
		Scan(&id)
	return
}

func (r *repository) GetByID(id int) (w *models.Worker, err error) {
	w = &models.Worker{}
	err = r.db.QueryRow(`SELECT worker_code, worker_name, factory_code FROM dim_worker WHERE worker_code = $1`, id).
		Scan(&w.Code, &w.Name, &w.FactoryCode)
	if err == sql.ErrNoRows {
		return nil, nil
	}
	return
}

func (r *repository) GetByFactoryCode(id int) (w []*models.Worker, err error) {
	rows, err := r.db.Query(`SELECT worker_code, worker_name, factory_code FROM dim_worker WHERE factory_code = $1`, id)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var data models.Worker
		err = rows.Scan(&data.Code, &data.Name, &data.FactoryCode)
		if err != nil {
			return nil, err
		}
		w = append(w, &data)
	}
	return
}

func (r *repository) GetAll() (w []*models.Worker, err error) {
	rows, err := r.db.Query(`SELECT worker_code, worker_name, factory_code FROM dim_worker`)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var data models.Worker
		err = rows.Scan(&data.Code, &data.Name, &data.FactoryCode)
		if err != nil {
			return nil, err
		}
		w = append(w, &data)
	}
	return
}

func (r *repository) Delete(id int) (err error) {
	result, err := r.db.Exec(`DELETE FROM dim_worker WHERE worker_code = $1`, id)
	if count, _ := result.RowsAffected(); count == 0 {
		return fmt.Errorf("no rows found")
	}
	return
}