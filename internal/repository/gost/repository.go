package gost

import (
	"database/sql"
	"fmt"
	"practice_4/internal/models"
)

type Repository interface {
	Create(g models.Gost) (id int, err error)
	GetByID(id int) (g *models.Gost, err error)
	GetByName(name string) (g *models.Gost, err error)
	GetAll() (g []*models.Gost, err error)
	Delete(id int) (err error)
}

type repository struct {
	db *sql.DB
}

func New(db *sql.DB) Repository {
	return &repository{
		db: db,
	}
}

func (r *repository) Create(g models.Gost) (id int, err error) {
	err = r.db.QueryRow(`INSERT INTO dim_gost(gost_description) VALUES($1) RETURNING gost_id`, g.Description).
		Scan(&id)
	return
}

func (r *repository) GetByID(id int) (g *models.Gost, err error) {
	g = &models.Gost{}
	err = r.db.QueryRow(`SELECT gost_id, gost_description FROM dim_gost WHERE gost_id = $1`, id).
		Scan(&g.ID, &g.Description)
	if err == sql.ErrNoRows {
		return nil, nil
	}
	return
}

func (r *repository) GetByName(name string) (g *models.Gost, err error) {
	g = &models.Gost{}
	err = r.db.QueryRow(`SELECT gost_id, gost_description FROM dim_gost WHERE gost_description = $1`, name).
		Scan(&g.ID, &g.Description)
	if err == sql.ErrNoRows {
		return nil, nil
	}
	return
}

func (r *repository) GetAll() (g []*models.Gost, err error) {
	rows, err := r.db.Query(`SELECT gost_id, gost_description FROM dim_gost`)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var data models.Gost
		err = rows.Scan(&data.ID, &data.Description)
		if err != nil {
			return nil, err
		}
		g = append(g, &data)
	}
	return
}

func (r *repository) Delete(id int) (err error) {
	result, err := r.db.Exec(`DELETE FROM dim_gost WHERE gost_id = $1`, id)
	if count, _ := result.RowsAffected(); count == 0 {
		return fmt.Errorf("no rows found")
	}
	return
}
