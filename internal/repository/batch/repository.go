package batch

import (
	"database/sql"
	"fmt"
	"practice_4/internal/models"
)

type Repository interface {
	Create(b models.Batch) (id int, err error)
	GetByID(id int) (b *models.Batch, err error)
	GetAll() (b []*models.Batch, err error)
	Delete(id int) (err error)
}

type repository struct {
	db *sql.DB
}

func New(db *sql.DB) Repository {
	return &repository{
		db: db,
	}
}

func (r *repository) Create(b models.Batch) (id int, err error) {
	err = r.db.QueryRow(`INSERT INTO dim_batch(batch_name) VALUES($1) RETURNING batch_code`, b.Name).
		Scan(&id)
	return
}

func (r *repository) GetByID(id int) (b *models.Batch, err error) {
	b = &models.Batch{}
	err = r.db.QueryRow(`SELECT batch_code, batch_name FROM dim_batch WHERE batch_code = $1`, id).
		Scan(&b.Code, &b.Name)
	if err == sql.ErrNoRows {
		return nil, nil
	}
	return
}

func (r *repository) GetAll() (b []*models.Batch, err error) {
	rows, err := r.db.Query(`SELECT batch_code, batch_name FROM dim_batch`)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var data models.Batch
		err = rows.Scan(&data.Code, &data.Name)
		if err != nil {
			return nil, err
		}
		b = append(b, &data)
	}
	return
}

func (r *repository) Delete(id int) (err error) {
	result, err := r.db.Exec(`DELETE FROM dim_batch WHERE batch_code = $1`, id)
	if count, _ := result.RowsAffected(); count == 0 {
		return fmt.Errorf("no rows found")
	}
	return
}