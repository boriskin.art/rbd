package machine

import (
	"database/sql"
	"fmt"
	"practice_4/internal/models"
)

type Repository interface {
	Create(m models.Machine) (id int, err error)
	GetByID(id int) (m *models.Machine, err error)
	GetAll() (m []*models.Machine, err error)
	Delete(id int) (err error)
}

type repository struct {
	db *sql.DB
}

func New(db *sql.DB) Repository {
	return &repository{
		db: db,
	}
}

func (r *repository) Create(m models.Machine) (id int, err error) {
	err = r.db.QueryRow(`INSERT INTO dim_machine(date_of_purchase, machine_name, machine_type_code) VALUES($1, $2, $3) RETURNING machine_code`, m.DateOfPurchase, m.Name, m.TypeCode).
		Scan(&id)
	return
}

func (r *repository) GetByID(id int) (m *models.Machine, err error) {
	m = &models.Machine{}
	err = r.db.QueryRow(`SELECT machine_code, date_of_purchase, machine_name, machine_type_code FROM dim_machine WHERE machine_code = $1`, id).
		Scan(&m.Code, &m.DateOfPurchase, &m.Name, &m.TypeCode)
	if err == sql.ErrNoRows {
		return nil, nil
	}
	return
}

func (r *repository) GetAll() (m []*models.Machine, err error) {
	rows, err := r.db.Query(`SELECT machine_code, date_of_purchase, machine_name, machine_type_code FROM dim_machine`)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var data models.Machine
		err = rows.Scan(&data.Code, &data.DateOfPurchase, &data.Name, &data.TypeCode)
		if err != nil {
			return nil, err
		}
		m = append(m, &data)
	}
	return
}

func (r *repository) Delete(id int) (err error) {
	result, err := r.db.Exec(`DELETE FROM dim_machine WHERE machine_code = $1`, id)
	if count, _ := result.RowsAffected(); count == 0 {
		return fmt.Errorf("no rows found")
	}
	return
}