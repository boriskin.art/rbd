package machine_type

import (
	"database/sql"
	"fmt"
	"practice_4/internal/models"
)

type Repository interface {
	Create(mt models.MachineType) (id int, err error)
	GetByID(id int) (mt *models.MachineType, err error)
	GetAll() (mt []*models.MachineType, err error)
	Delete(id int) (err error)
}

type repository struct {
	db *sql.DB
}

func New(db *sql.DB) Repository {
	return &repository{
		db: db,
	}
}

func (r *repository) Create(mt models.MachineType) (id int, err error) {
	err = r.db.QueryRow(`INSERT INTO dim_machine_type(machine_type_description) VALUES($1) RETURNING machine_type_code`, mt.TypeName).
		Scan(&id)
	return
}

func (r *repository) GetByID(id int) (mt *models.MachineType, err error) {
	mt = &models.MachineType{}
	err = r.db.QueryRow(`SELECT machine_type_code, machine_type_description FROM dim_machine_type WHERE machine_type_code = $1`, id).
		Scan(&mt.TypeCode, &mt.TypeName)
	if err == sql.ErrNoRows {
		return nil, nil
	}
	return
}

func (r *repository) GetAll() (mt []*models.MachineType, err error) {
	rows, err := r.db.Query(`SELECT machine_type_code, machine_type_description FROM dim_machine_type`)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var data models.MachineType
		err = rows.Scan(&data.TypeCode, &data.TypeName)
		if err != nil {
			return nil, err
		}
		mt = append(mt, &data)
	}
	return
}

func (r *repository) Delete(id int) (err error) {
	result, err := r.db.Exec(`DELETE FROM dim_machine_type WHERE machine_type_code = $1`, id)
	if count, _ := result.RowsAffected(); count == 0 {
		return fmt.Errorf("no rows found")
	}
	return
}