package fact_manufacturing

import (
	"database/sql"
	"fmt"
	"practice_4/internal/models"
)

type Repository interface {
	Create(fm models.FactManufacturing) (id int, err error)
	GetByID(id int) (fm *models.FactManufacturing, err error)
	GetByBatchID(id int) (fm []*models.FactManufacturing, err error)
	GetAll() (fm []*models.FactManufacturing, err error)
	Delete(id int) (err error)
}

type repository struct {
	db *sql.DB
}

func New(db *sql.DB) Repository {
	return &repository{
		db: db,
	}
}

func (r *repository) Create(fm models.FactManufacturing) (id int, err error) {
	err = r.db.QueryRow(`
		INSERT INTO fact_manufacturing(
		    batch_code, furniture_code, machine_code, accepted_products, rejected_products, elapsed_time, gost_id
		) VALUES($1, $2, $3, $4, $5, $6, $7) RETURNING id`,
		fm.BatchCode, fm.FurnitureCode, fm.MachineCode, fm.AcceptedProducts,
		fm.RejectedProducts, fm.ElapsedTime, fm.GostID,
	).Scan(&id)

	return
}

func (r *repository) GetByID(id int) (fm *models.FactManufacturing, err error) {
	fm = &models.FactManufacturing{}
	err = r.db.QueryRow(`SELECT 
       		id, 
       		batch_code, 
       		furniture_code,
       		machine_code, 
       		accepted_products, 
       		rejected_products, 
       		elapsed_time, 
       		gost_id
		FROM fact_manufacturing WHERE id = $1`, id,
	).Scan(
		&fm.ID, &fm.BatchCode, &fm.FurnitureCode, &fm.MachineCode,
		&fm.AcceptedProducts, &fm.RejectedProducts, &fm.ElapsedTime, &fm.GostID,
	)
	if err == sql.ErrNoRows {
		return nil, nil
	}
	return
}

func (r *repository) GetByBatchID(id int) (fm []*models.FactManufacturing, err error) {
	rows, err := r.db.Query(`
		SELECT 
		       id, batch_code, furniture_code, machine_code, 
		       accepted_products, rejected_products, elapsed_time, gost_id 
		FROM fact_manufacturing 
		WHERE batch_code = $1`, id,
	)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var data models.FactManufacturing
		err = rows.Scan(&data.ID, &data.BatchCode, &data.FurnitureCode, &data.MachineCode,
			&data.AcceptedProducts, &data.RejectedProducts, &data.ElapsedTime, &data.GostID,
		)
		if err != nil {
			return nil, err
		}
		fm = append(fm, &data)
	}
	return
}

func (r *repository) GetAll() (fm []*models.FactManufacturing, err error) {
	rows, err := r.db.Query(`SELECT id, batch_code, furniture_code, machine_code, accepted_products, rejected_products, elapsed_time, gost_id FROM fact_manufacturing`)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var data models.FactManufacturing
		err = rows.Scan(&data.ID, &data.BatchCode, &data.FurnitureCode, &data.MachineCode, &data.AcceptedProducts, &data.RejectedProducts, &data.ElapsedTime, &data.GostID)
		if err != nil {
			return nil, err
		}
		fm = append(fm, &data)
	}
	return
}

func (r *repository) Delete(id int) (err error) {
	result, err := r.db.Exec(`DELETE FROM fact_manufacturing WHERE id = $1`, id)
	if count, _ := result.RowsAffected(); count == 0 {
		return fmt.Errorf("no rows found")
	}
	return
}