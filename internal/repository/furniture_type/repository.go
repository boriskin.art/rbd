package furniture_type

import (
	"database/sql"
	"fmt"
	"practice_4/internal/models"
)

type Repository interface {
	Create(ft models.FurnitureType) (id int, err error)
	GetByID(id int) (ft *models.FurnitureType, err error)
	GetByName(name string) (ft *models.FurnitureType, err error)
	GetAll() (ft []*models.FurnitureType, err error)
	Delete(id int) (err error)
}

type repository struct {
	db *sql.DB
}

func New(db *sql.DB) Repository {
	return &repository{
		db: db,
	}
}

func (r *repository) Create(ft models.FurnitureType) (id int, err error) {
	err = r.db.QueryRow(`INSERT INTO dim_furniture_type(furniture_type_name) VALUES($1) RETURNING furniture_type_code`, ft.TypeName).
		Scan(&id)
	return
}

func (r *repository) GetByID(id int) (ft *models.FurnitureType, err error) {
	ft = &models.FurnitureType{}
	err = r.db.QueryRow(`SELECT furniture_type_code, furniture_type_name FROM dim_furniture_type WHERE furniture_type_code = $1`, id).
		Scan(&ft.TypeCode, &ft.TypeName)
	if err == sql.ErrNoRows {
		return nil, nil
	}
	return
}

func (r *repository) GetByName(name string) (ft *models.FurnitureType, err error) {
	ft = &models.FurnitureType{}
	err = r.db.QueryRow(`SELECT furniture_type_code, furniture_type_name FROM dim_furniture_type WHERE furniture_type_name = $1`, name).
		Scan(&ft.TypeCode, &ft.TypeName)
	if err == sql.ErrNoRows {
		return nil, nil
	}
	return
}

func (r *repository) GetAll() (ft []*models.FurnitureType, err error) {
	rows, err := r.db.Query(`SELECT furniture_type_code, furniture_type_name FROM dim_furniture_type`)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var data models.FurnitureType
		err = rows.Scan(&data.TypeCode, &data.TypeName)
		if err != nil {
			return nil, err
		}
		ft = append(ft, &data)
	}
	return
}

func (r *repository) Delete(id int) (err error) {
	result, err := r.db.Exec(`DELETE FROM dim_furniture_type WHERE furniture_type_code = $1`, id)
	if count, _ := result.RowsAffected(); count == 0 {
		return fmt.Errorf("no rows found")
	}
	return
}
