package factory

import (
	"database/sql"
	"fmt"
	"practice_4/internal/models"
)

type Repository interface {
	Create(f models.Factory) (id int, err error)
	GetByID(id int) (f *models.Factory, err error)
	GetAll() (f []*models.Factory, err error)
	Delete(id int) (err error)
}

type repository struct {
	db *sql.DB
}

func New(db *sql.DB) Repository {
	return &repository{
		db: db,
	}
}

func (r *repository) Create(f models.Factory) (id int, err error) {
	err = r.db.QueryRow(`INSERT INTO dim_factory(factory_name, country_code) VALUES($1, $2) RETURNING factory_code`, f.Name, f.CountryCode).
		Scan(&id)
	return
}

func (r *repository) GetByID(id int) (f *models.Factory, err error) {
	f = &models.Factory{}
	err = r.db.QueryRow(`SELECT factory_code, factory_name, country_code FROM dim_factory WHERE factory_code = $1`, id).
		Scan(&f.Code, &f.Name, &f.CountryCode)
	if err == sql.ErrNoRows {
		return nil, nil
	}
	return
}

func (r *repository) GetAll() (f []*models.Factory, err error) {
	rows, err := r.db.Query(`SELECT factory_code, factory_name, country_code FROM dim_factory`)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var data models.Factory
		err = rows.Scan(&data.Code, &data.Name, &data.CountryCode)
		if err != nil {
			return nil, err
		}
		f = append(f, &data)
	}
	return
}

func (r *repository) Delete(id int) (err error) {
	result, err := r.db.Exec(`DELETE FROM dim_factory WHERE factory_code = $1`, id)
	if count, _ := result.RowsAffected(); count == 0 {
		return fmt.Errorf("no rows found")
	}
	return
}