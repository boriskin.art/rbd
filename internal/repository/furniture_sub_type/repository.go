package furniture_sub_type

import (
	"database/sql"
	"fmt"
	"practice_4/internal/models"
)

type Repository interface {
	Create(fst models.FurnitureSubType) (id int, err error)
	GetByID(id int) (fst *models.FurnitureSubType, err error)
	GetByName(name string) (fst *models.FurnitureSubType, err error)
	GetByTypeID(id int) (fst []*models.FurnitureSubType, err error)
	GetAll() (fst []*models.FurnitureSubType, err error)
	Delete(id int) (err error)
}

type repository struct {
	db *sql.DB
}

func New(db *sql.DB) Repository {
	return &repository{
		db: db,
	}
}

func (r *repository) Create(fst models.FurnitureSubType) (id int, err error) {
	err = r.db.QueryRow(`INSERT INTO dim_furniture_sub_type(furniture_sub_type_name, furniture_type_code) VALUES($1, $2) RETURNING furniture_sub_type_code`, fst.SubTypeName, fst.TypeCode).
		Scan(&id)
	return
}

func (r *repository) GetByID(id int) (fst *models.FurnitureSubType, err error) {
	fst = &models.FurnitureSubType{}
	err = r.db.QueryRow(`SELECT furniture_sub_type_code, furniture_sub_type_name, furniture_type_code FROM dim_furniture_sub_type WHERE furniture_sub_type_code = $1`, id).
		Scan(&fst.SubTypeCode, &fst.SubTypeName, &fst.TypeCode)
	if err == sql.ErrNoRows {
		return nil, nil
	}
	return
}

func (r *repository) GetByName(name string) (fst *models.FurnitureSubType, err error) {
	fst = &models.FurnitureSubType{}
	err = r.db.QueryRow(`SELECT furniture_sub_type_code, furniture_sub_type_name, furniture_type_code FROM dim_furniture_sub_type WHERE furniture_sub_type_name = $1`, name).
		Scan(&fst.SubTypeCode, &fst.SubTypeName, &fst.TypeCode)
	if err == sql.ErrNoRows {
		return nil, nil
	}
	return
}

func (r *repository) GetByTypeID(id int) (fst []*models.FurnitureSubType, err error) {
	rows, err := r.db.Query(`SELECT furniture_sub_type_code, furniture_sub_type_name, furniture_type_code FROM dim_furniture_sub_type WHERE furniture_type_code = $1`, id)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var f models.FurnitureSubType
		err = rows.Scan(&f.SubTypeCode, &f.SubTypeName, &f.TypeCode)
		if err != nil {
			return nil, err
		}
		fst = append(fst, &f)
	}
	return
}

func (r *repository) GetAll() (fst []*models.FurnitureSubType, err error) {
	rows, err := r.db.Query(`SELECT furniture_sub_type_code, furniture_sub_type_name, furniture_type_code FROM dim_furniture_sub_type`)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var f models.FurnitureSubType
		err = rows.Scan(&f.SubTypeCode, &f.SubTypeName, &f.TypeCode)
		if err != nil {
			return nil, err
		}
		fst = append(fst, &f)
	}
	return
}

func (r *repository) Delete(id int) (err error) {
	result, err := r.db.Exec(`DELETE FROM dim_furniture_sub_type WHERE furniture_sub_type_code = $1`, id)
	if count, _ := result.RowsAffected(); count == 0 {
		return fmt.Errorf("no rows found")
	}
	return
}
