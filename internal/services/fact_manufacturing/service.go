package fact_manufacturing

import (
	"practice_4/internal/models"
	"practice_4/internal/repository/fact_manufacturing"
)

type Service interface {
	Create(fm models.FactManufacturing) (id int, err error)
	GetByID(id int) (fm *models.FactManufacturing, err error)
	GetByBatchID(id int) (fm []*models.FactManufacturing, err error)
	GetAll() (fm []*models.FactManufacturing, err error)
	Delete(id int) (err error)
}

type service struct {
	factManufacturingRepo fact_manufacturing.Repository
}

func New(factManufacturingRepo fact_manufacturing.Repository) Service {
	return &service{
		factManufacturingRepo: factManufacturingRepo,
	}
}

func (s *service) Create(fm models.FactManufacturing) (id int, err error) {
	return s.factManufacturingRepo.Create(fm)
}

func (s *service) GetByID(id int) (fm *models.FactManufacturing, err error) {
	return s.factManufacturingRepo.GetByID(id)
}

func (s *service) GetByBatchID(id int) (fm []*models.FactManufacturing, err error) {
	return s.factManufacturingRepo.GetByBatchID(id)
}

func (s *service) GetAll() (fm []*models.FactManufacturing, err error) {
	return s.factManufacturingRepo.GetAll()
}

func (s *service) Delete(id int) (err error) {
	return s.factManufacturingRepo.Delete(id)
}
