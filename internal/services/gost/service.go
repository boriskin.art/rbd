package gost

import (
	"practice_4/internal/models"
	"practice_4/internal/repository/gost"
)

type Service interface {
	Create(g models.Gost) (id int, err error)
	GetByID(id int) (g *models.Gost, err error)
	GetByName(name string) (g *models.Gost, err error)
	GetAll() (g []*models.Gost, err error)
	Delete(id int) (err error)
}

type service struct {
	gostRepo gost.Repository
}

func New(gostRepo gost.Repository) Service {
	return &service{
		gostRepo: gostRepo,
	}
}

func (s *service) Create(g models.Gost) (id int, err error) {
	return s.gostRepo.Create(g)
}

func (s *service) GetByID(id int) (g *models.Gost, err error) {
	return s.gostRepo.GetByID(id)
}

func (s *service) GetByName(name string) (g *models.Gost, err error) {
	return s.gostRepo.GetByName(name)
}

func (s *service) GetAll() (g []*models.Gost, err error) {
	return s.gostRepo.GetAll()
}

func (s *service) Delete(id int) (err error) {
	return s.gostRepo.Delete(id)
}
