package machine_type

import (
	"practice_4/internal/models"
	"practice_4/internal/repository/machine_type"
)

type Service interface {
	Create(mt models.MachineType) (id int, err error)
	GetByID(id int) (mt *models.MachineType, err error)
	GetAll() (mt []*models.MachineType, err error)
	Delete(id int) (err error)
}

type service struct {
	machineTypeRepo machine_type.Repository
}

func New(machineTypeRepo machine_type.Repository) Service {
	return &service{
		machineTypeRepo: machineTypeRepo,
	}
}

func (s *service) Create(mt models.MachineType) (id int, err error) {
	return s.machineTypeRepo.Create(mt)
}

func (s *service) GetByID(id int) (mt *models.MachineType, err error) {
	return s.machineTypeRepo.GetByID(id)
}

func (s *service) GetAll() (mt []*models.MachineType, err error) {
	return s.machineTypeRepo.GetAll()
}

func (s *service) Delete(id int) (err error) {
	return s.machineTypeRepo.Delete(id)
}
