package batch

import (
	"practice_4/internal/models"
	"practice_4/internal/repository/batch"
)

type Service interface {
	Create(b models.Batch) (id int, err error)
	GetByID(id int) (b *models.Batch, err error)
	GetAll() (b []*models.Batch, err error)
	Delete(id int) (err error)
}

type service struct {
	batchRepo batch.Repository
}

func New(batchRepo batch.Repository) Service {
	return &service{
		batchRepo: batchRepo,
	}
}

func (s *service) Create(b models.Batch) (id int, err error) {
	return s.batchRepo.Create(b)
}

func (s *service) GetByID(id int) (b *models.Batch, err error) {
	return s.batchRepo.GetByID(id)
}

func (s *service) GetAll() (b []*models.Batch, err error) {
	return s.batchRepo.GetAll()
}

func (s *service) Delete(id int) (err error) {
	return s.batchRepo.Delete(id)
}
