package factory

import (
	"practice_4/internal/models"
	"practice_4/internal/repository/factory"
)

type Service interface {
	Create(f models.Factory) (id int, err error)
	GetByID(id int) (f *models.Factory, err error)
	GetAll() (f []*models.Factory, err error)
	Delete(id int) (err error)
}

type service struct {
	factoryRepo factory.Repository
}

func New(factoryRepo factory.Repository) Service {
	return &service{
		factoryRepo: factoryRepo,
	}
}

func (s *service) Create(f models.Factory) (id int, err error) {
	return s.factoryRepo.Create(f)
}

func (s *service) GetByID(id int) (f *models.Factory, err error) {
	return s.factoryRepo.GetByID(id)
}

func (s *service) GetAll() (f []*models.Factory, err error) {
	return s.factoryRepo.GetAll()
}

func (s *service) Delete(id int) (err error) {
	return s.factoryRepo.Delete(id)
}
