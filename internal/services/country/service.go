package country

import (
	"practice_4/internal/models"
	"practice_4/internal/repository/country"
)

type Service interface {
	Create(c models.Country) (id int, err error)
	GetByID(id int) (c *models.Country, err error)
	GetByName(name string) (c *models.Country, err error)
	GetAll() (c []*models.Country, err error)
	Delete(id int) (err error)
}

type service struct {
	countryRepo country.Repository
}

func New(countryRepo country.Repository) Service {
	return &service{
		countryRepo: countryRepo,
	}
}

func (s *service) Create(c models.Country) (id int, err error) {
	return s.countryRepo.Create(c)
}

func (s *service) GetByID(id int) (c *models.Country, err error) {
	return s.countryRepo.GetByID(id)
}

func (s *service) GetByName(name string) (c *models.Country, err error) {
	return s.countryRepo.GetByName(name)
}

func (s *service) GetAll() (c []*models.Country, err error) {
	return s.countryRepo.GetAll()
}

func (s *service) Delete(id int) (err error) {
	return s.countryRepo.Delete(id)
}
