package furniture

import (
	"practice_4/internal/models"
	"practice_4/internal/repository/furniture"
)

type Service interface {
	Create(f models.Furniture) (id int, err error)
	GetByID(id int) (f *models.Furniture, err error)
	GetAll() (f []*models.Furniture, err error)
	Delete(id int) (err error)
}

type service struct {
	furnitureRepo furniture.Repository
}

func New(furnitureRepo furniture.Repository) Service {
	return &service{
		furnitureRepo: furnitureRepo,
	}
}

func (s *service) Create(f models.Furniture) (id int, err error) {
	return s.furnitureRepo.Create(f)
}

func (s *service) GetByID(id int) (f *models.Furniture, err error) {
	return s.furnitureRepo.GetByID(id)
}

func (s *service) GetAll() (f []*models.Furniture, err error) {
	return s.furnitureRepo.GetAll()
}

func (s *service) Delete(id int) (err error) {
	return s.furnitureRepo.Delete(id)
}
