package furniture_type

import (
	"practice_4/internal/models"
	"practice_4/internal/repository/furniture_type"
)

type Service interface {
	Create(ft models.FurnitureType) (id int, err error)
	GetByID(id int) (ft *models.FurnitureType, err error)
	GetByName(name string) (ft *models.FurnitureType, err error)
	GetAll() (ft []*models.FurnitureType, err error)
	Delete(id int) (err error)
}

type service struct {
	furnitureTypeRepo furniture_type.Repository
}

func New(furnitureTypeRepo furniture_type.Repository) Service {
	return &service{
		furnitureTypeRepo: furnitureTypeRepo,
	}
}

func (s *service) Create(ft models.FurnitureType) (id int, err error) {
	return s.furnitureTypeRepo.Create(ft)
}

func (s *service) GetByID(id int) (ft *models.FurnitureType, err error) {
	return s.furnitureTypeRepo.GetByID(id)
}

func (s *service) GetByName(name string) (ft *models.FurnitureType, err error) {
	return s.furnitureTypeRepo.GetByName(name)
}

func (s *service) GetAll() (ft []*models.FurnitureType, err error) {
	return s.furnitureTypeRepo.GetAll()
}

func (s *service) Delete(id int) (err error) {
	return s.furnitureTypeRepo.Delete(id)
}
