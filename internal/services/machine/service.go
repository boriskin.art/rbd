package machine

import (
	"practice_4/internal/models"
	"practice_4/internal/repository/machine"
)

type Service interface {
	Create(m models.Machine) (id int, err error)
	GetByID(id int) (m *models.Machine, err error)
	GetAll() (m []*models.Machine, err error)
	Delete(id int) (err error)
}

type service struct {
	machineRepo machine.Repository
}

func New(machineRepo machine.Repository) Service {
	return &service{
		machineRepo: machineRepo,
	}
}

func (s *service) Create(m models.Machine) (id int, err error) {
	return s.machineRepo.Create(m)
}

func (s *service) GetByID(id int) (m *models.Machine, err error) {
	return s.machineRepo.GetByID(id)
}

func (s *service) GetAll() (m []*models.Machine, err error) {
	return s.machineRepo.GetAll()
}

func (s *service) Delete(id int) (err error) {
	return s.machineRepo.Delete(id)
}
