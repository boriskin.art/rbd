package worker

import (
	"practice_4/internal/models"
	"practice_4/internal/repository/worker"
)

type Service interface {
	Create(w models.Worker) (id int, err error)
	GetByID(id int) (w *models.Worker, err error)
	GetByFactoryID(id int) (w []*models.Worker, err error)
	GetAll() (w []*models.Worker, err error)
	Delete(id int) (err error)
}

type service struct {
	workerRepo worker.Repository
}

func New(workerRepo worker.Repository) Service {
	return &service{
		workerRepo: workerRepo,
	}
}

func (s *service) Create(w models.Worker) (id int, err error) {
	return s.workerRepo.Create(w)
}

func (s *service) GetByID(id int) (w *models.Worker, err error) {
	return s.workerRepo.GetByID(id)
}

func (s *service) GetByFactoryID(id int) (w []*models.Worker, err error) {
	return s.workerRepo.GetByFactoryCode(id)
}

func (s *service) GetAll() (c []*models.Worker, err error) {
	return s.workerRepo.GetAll()
}

func (s *service) Delete(id int) (err error) {
	return s.workerRepo.Delete(id)
}
