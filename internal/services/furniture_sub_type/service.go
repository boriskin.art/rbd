package furniture_sub_type

import (
	"practice_4/internal/models"
	"practice_4/internal/repository/furniture_sub_type"
)

type Service interface {
	Create(fst models.FurnitureSubType) (id int, err error)
	GetByID(id int) (fst *models.FurnitureSubType, err error)
	GetByName(name string) (fst *models.FurnitureSubType, err error)
	GetByTypeID(id int) (fst []*models.FurnitureSubType, err error)
	GetAll() (fst []*models.FurnitureSubType, err error)
	Delete(id int) (err error)
}

type service struct {
	furnitureSubTypeRepo furniture_sub_type.Repository
}

func New(furnitureSubTypeRepo furniture_sub_type.Repository) Service {
	return &service{
		furnitureSubTypeRepo: furnitureSubTypeRepo,
	}
}

func (s *service) Create(fst models.FurnitureSubType) (id int, err error) {
	return s.furnitureSubTypeRepo.Create(fst)
}

func (s *service) GetByID(id int) (fst *models.FurnitureSubType, err error) {
	return s.furnitureSubTypeRepo.GetByID(id)
}

func (s *service) GetByName(name string) (fst *models.FurnitureSubType, err error) {
	return s.furnitureSubTypeRepo.GetByName(name)
}

func (s *service) GetByTypeID(id int) (fst []*models.FurnitureSubType, err error) {
	return s.furnitureSubTypeRepo.GetByTypeID(id)
}

func (s *service) GetAll() (fst []*models.FurnitureSubType, err error) {
	return s.furnitureSubTypeRepo.GetAll()
}

func (s *service) Delete(id int) (err error) {
	return s.furnitureSubTypeRepo.Delete(id)
}
