package internal

type Response struct {
	Status string      `json:"status"`
	Result interface{} `json:"result"`
}

type Result struct {
	ID int `json:"id"`
}
